<?php 
	function http_init($url)
	{
		$port = getservbyname('www', 'tcp');
		$pattern = '/^(?:https?:\/\/)?((?:[\w\.-]+\.[a-z]{2,6})|(?:(?:[0-9]{1,3}\.){3}(?:[0-9]{1,3})))(?:\/[\/\w \.-]*)?(?:(?:\?|\#).*)?$/i';
		if (!preg_match($pattern, $url, $matches))
		{
			echo "failed URL\r\n";
			return false;
		}
		$address = $matches[1];
		$socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
		if ($socket === false)
		{
			echo "socket_create() failed: reason: " . socket_strerror(socket_last_error()) . "\r\n";
			return false;
		}
		$result = socket_connect($socket, $address, $port);
		if ($result === false)
		{
			echo "socket_connect() failed: reason: " . socket_strerror(socket_last_error($socket)) . "\r\n";
			return false;
		}
		return $socket;
	}
	function http_uninit($handle)
	{
		socket_close($handle);
	}
	function http_read($handle, $only_header)
	{
		$answer['data'] = '';
		$in_data = false;
		$offset = 0;
		$size = 0;
		$out = '';

		do
		{
			$out .= socket_read($handle, 1);
			if (preg_match('/\\r\\n\\r(\\n)/', $out, $matches, PREG_OFFSET_CAPTURE))
			{
				if ($only_header === true)
				{
					break;
				}
				$in_data = true;
				$answer['data'] .= $out;
				$answer['offset_data'] = $matches[1][1];
				break;
			}
		}
		while (true);
		//parsing head
		return $answer;

		do
		{
			$out .= socket_read($handle, 50);
			echo $out."<br>\r\n";
			if ($out === false)
			{
				echo "socket_read() failed: reason: " . socket_strerror(socket_last_error($handle)) . "\r\n";
				break;
			}
			if ($out == "")
			{
				echo "end data\r\n";
				break;
			}
			if ($in_data === false)
			{
				$lines = preg_split('/\\r\\n?|\\n/', $out, null, PREG_SPLIT_OFFSET_CAPTURE); 
				for ($i = 0; $i < count($lines); $i++) 
				{
					//message-header
					if (preg_match('/^\s*([\w-]*):\s*(.*)\s*$/i', $lines[$i][0], $matches))
					{
						$answer[$matches[1]] = $matches[2];
					}
					else
					//CRLF
					if (preg_match('/^\s*$/', $lines[$i][0]))
					{
						$size = $answer['Content-Length'];
						if (($i + 1) < count($lines))
						{
							$offset = $lines[$i + 1][1];
						}
						else
						{
							$offset = mb_strlen($out);
						}
						break;
					}
					else
					//start-line
					if (preg_match('/^HTTP\/1.[01] ([0-9]{3}) ([\w- ]*)\s*$/i', $lines[$i][0], $matches))
					{
						$answer['status_code'] = $matches[1];
						$answer['status_text'] = $matches[2];
					}

				}
				if ($offset)
				{
					$in_data = true;
					if ($offset >= mb_strlen($out))
					{
						continue;
					}
				}
			}
			if ($in_data === true)
			{				
				$str = substr($out, $offset);
				$answer['data'] .= $str;
				$size -= strlen($str);
				if ($size <= 0)
				{
					break;
				}
				$offset = 0;		
				
			}
		}
		while (true);
		return $answer;
	}
	function http_write($handle, $buf)
	{
		return socket_write($handle, $buf, strlen($buf));
	}
	
	$address = "192.168.5.211";
	$file = "/ML3/Logs/rn_protocol.log";

	ob_implicit_flush();
	if (false === ($handle = http_init($address)))
	{
		echo "http_init() failed: reason: " . socket_strerror(socket_last_error($handle)) . "\r\n";
		return -1;
	}

/*
	//send request HEAD and get answer 
	$in = "HEAD $file HTTP/1.1\r\n";
	$in .= "Host: $address\r\n";
	$in .= "Cache-Control: no-cache\r\n";
	$in .= "Connection: keep-alive\r\n\r\n";
	if (false === http_write($handle, $in))
	{
		echo "socket_write() failed: reason: " . socket_strerror(socket_last_error($handle)) . "\r\n";
		return -1;
	}
	print_r(http_read($handle, true));
 */
	echo "<br>\r\n";

	//send request accept-ranges GET and get answer
	$in = "GET $file HTTP/1.1\r\n";
	$in .= "Host: $address\r\n";
	$in .= "Cache-Control: no-cache\r\n";
	$in .= "Range: bytes=0-1000\r\n";
	$in .= "Connection: keep-alive\r\n\r\n";
	if (false === http_write($handle, $in))
	{
		echo "http_write() failed: reason: " . socket_strerror(socket_last_error($handle)) . "\r\n";
		return -1;
	}
	print_r(http_read($handle, false));

	http_uninit($handle);
?>
